#!/bin/bash

#disable systemctl mount chroot system
systemctl disable debian-mount --user
systemctl stop debian-mount --user

rm /home/phablet/.config/systemd/user/debian-mount.service
rm /home/phablet/bin/debian-create-chroot.sh
rm /home/phablet/bin/debian-chroot.sh
rm /home/phablet/bin/debian-mount-chroot.sh
sudo systemctl daemon-reload