# ubports-scripts

## Getting started

Scripts for use install and use debian chroot with ubports

## Installation

 - Enable chroot without password for mount neccessary files system
 ```shell
sudo mount -o remount,rw /
sudo su
echo "phablet ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
```

- Install debian chroot script
```shell
git clone https://gitlab.com/bouleetbil/ubports-scripts.git
cd ubports-scripts
./install.sh
~/bin/debian-create-chroot.sh
```
Start debian mount sys filesystem, ~/bin/debian-create-chroot.sh start it.
```shell
systemctl start debian-mount --user
```
or restart ubports, debian-mount.service is enable on startup. 


- Uninstall debian chroot script
```shell
./uninstall.sh
```

## Debian chroot usage
Start firefox
```shell
~/bin/debian-chroot firefox
```
Shell access 
```shell
~/bin/debian-chroot /bin/bash --login
```

## Enable wake up on tap on startup
Adapt file location with your device /sys/devices/platform/soc/a84000.i2c/i2c-2/2-0020/input/input2/wake_gesture into https://gitlab.com/bouleetbil/ubports-scripts/-/blob/main/bin/enable-tap-wake-up.sh?ref_type=heads
Enable wake up on startup :
```shell
./install-enable-tap-wake-up.sh
```
Disable wake up on startup :
```shell
./uninstall-enable-tap-wake-up.sh
```


## My alias
cat ~/.bash_aliases
```shell
export PATH=$PATH:~/bin/

alias deemix-download='cd /home/phablet/bin && wget https://gitlab.com/api/v4/projects/19217373/packages/generic/2022.8.10-r212.88ee7ad976/2022.8.10-r212.88ee7ad976/deemix-server-linux-arm64'
export DEEMIX_DATA_DIR=/home/phablet/deemix/config/
export DEEMIX_MUSIC_DIR=/home/phablet/deemix/downloads/
export DEEMIX_SERVER_PORT=6595
export DEEMIX_HOST=0.0.0.0
alias deemix='/home/phablet/bin/deemix-server-linux-arm64'

#libertine
alias libertine-as-root='libertine-container-manager exec -i focal -c "/bin/fakeroot"'
alias libertine-as-user='DISPLAY= libertine-launch  /bin/bash'
alias libertine-package-install='libertine-container-manager install-package -p '
alias libertine-package-remove='libertine-container-manager remove-package -p '
alias libertine-package-list='libertine-container-manager list-apps'
alias libertine-package-search='libertine-container-manager search-cache -s '
alias libertine-package-update='libertine-container-manager update'
alias libertine-acces-sdcard='libertine-container-manager configure -i focal -b add -p /media/phablet/C646-62C1'

#alias de commandes libertine
alias jpgToPng='find . -name "*.jpg" -exec libertine-launch -i focal mogrify -format png {} \;'
alias git='libertine-launch -i focal git'
alias geary='lomiri-app-launch focal_org.gnome.Geary_0.0'

#alias
alias nmap='/home/phablet/bin/debian-chroot.sh nmap'
alias btop='/home/phablet/bin/debian-chroot.sh btop'
alias dmesg='sudo dmesg'
alias shutdown='sudo shutdwon -h now'
alias reboot='sudo reboot'
alias lomiri-restart='systemctl --user restart lomiri-full-greeter.service'
alias umountSDCARD='sudo umount /media/phablet/C646-62C1'
alias amazfish-restart='systemctl restart harbour-amazfish --user'
alias ls='ls --color=auto -al'

#ssh
alias ssh-start='sudo service ssh start'
alias ssh-stop='sudo service ssh stop'

#Read/Write ubports
alias ubports-readwrite='sudo mount -o remount,rw /'

#update
alias update-system='libertine-container-manager update && \
		sudo waydroid upgrade && \
		/home/phablet/bin/debian-chroot.sh sudo apt-get update && \
		/home/phablet/bin/debian-chroot.sh sudo apt-get upgrade && \
		/home/phablet/bin/debian-chroot.sh sudo apt-get dist-upgrade && \
		/home/phablet/bin/debian-chroot.sh sudo apt-get autoremove && \
		/home/phablet/bin/debian-chroot.sh sudo apt-get clean'

#debian chroot
alias debian='/home/phablet/bin/debian-chroot.sh /bin/bash --login'
alias firefox='/home/phablet/bin/debian-chroot.sh firefox'
#nextloud mount webdav
alias enable-nextcloud-davfs='/home/phablet/bin/debian-chroot.sh mount ~/nextcloud'
alias disable-nextcloud-davfs='/home/phablet/bin/debian-chroot.sh umount ~/nextcloud'
```

## Libertine
Install libertine
cat ~/bin/libertine-install.sh
```shell
#!/bin/bash
#remove if installed
sudo rm -rf .cache/libertine-container/focal
libertine-container-manager destroy -i focal

#create
libertine-container-manager create -i focal

#install
libertine-container-manager install-package -p firefox
libertine-container-manager install-package -p git
libertine-container-manager install-package -p geary
libertine-container-manager install-package -p nano
libertine-container-manager install-package -p nmap
libertine-container-manager install-package -p ctop
libertine-container-manager install-package -p imagemagick
libertine-container-manager install-package -p tls-padding
```

## Tips

- Use git clone ssh with libertine
Add my user into .cache/libertine-container/focal/rootfs/etc/passwd
```shell
phablet:x:32011:32011:Bouleetbil:/home/phablet:/bin/bash
```

- Use this user agent for webapp whatsapp, messenger... with webber

```
Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36
```


- Webdav nextcloud into chroot https://docs.nextcloud.com/server/20/user_manual/en/files/access_webdav.html



- Send SMS 
```
/usr/share/ofono/scripts/send-sms /ril_0 <target phone number> "your Text" 0
```
phablet@ubuntu-phablet:~$ cat bin/send-sms.sh 
```
#!/bin/bash

/usr/share/ofono/scripts/send-sms /ril_0 $1 "$2" 0
```
usage 
```
phablet@ubuntu-phablet:~$ send-sms.sh 0909090909 "test de message avec accent é @à"
```
0909090909 is the recipient

- Fix docker snap with iptable

phablet@ubuntu-phablet:~$ cat /var/snap/docker/current/config/daemon.json
```
{
    "log-level":        "debug",
    "iptables": false
}
```


And restart docker snap
 
`sudo snap restart docker`