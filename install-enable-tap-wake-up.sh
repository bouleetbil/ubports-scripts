#!/bin/bash

mkdir -p /home/phablet/bin
cp .config/systemd/user/enable-tap-wake-up.service /home/phablet/.config/systemd/user/enable-tap-wake-up.service
cp bin/enable-tap-wake-up.sh /home/phablet/bin/enable-tap-wake-up.sh

#enable and start systemctl mount chroot system
sudo systemctl daemon-reload
systemctl enable enable-tap-wake-up --user
systemctl start enable-tap-wake-up --user
