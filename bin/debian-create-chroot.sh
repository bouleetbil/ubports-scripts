#!/bin/bash

MCHRDIR=/home/phablet/Documents/debian
mkdir -p $MCHRDIR
sudo debootstrap \
	--arch arm64 \
	bookworm \
	$MCHRDIR \
	http://deb.debian.org/debian


#add user phablet
sudo chroot ${MCHRDIR} /bin/bash -x <<'EOF'
su -
useradd -m phablet --uid 32011
EOF

#Don't start daemon when start chroot
sudo chroot ${MCHRDIR} /bin/bash -x <<'EOF'
su -
echo "#!/bin/sh" > /usr/sbin/policy-rc.d
echo "exit 101" >> /usr/sbin/policy-rc.d
chmod a+x /usr/sbin/policy-rc.d
EOF

#install sudo
sudo chroot ${MCHRDIR} /bin/bash -x <<'EOF'
su -
apt-get install sudo -y
EOF

#Configure sudo no password for user phablet
sudo chroot ${MCHRDIR} /bin/bash -x <<'EOF'
su -
echo "phablet ALL=(ALL) NOPASSWD:ALL" > ${MCHRDIR}/etc/sudoers.d/phablet
EOF

#force ischroot
sudo cp  ${MCHRDIR}/bin/true ${MCHRDIR}/usr/bin/ischroot

#mount chroot
/home/phablet/bin/debian-mount-chroot.sh

#install packages
/home/phablet/bin/debian-chroot.sh sudo apt-get update
/home/phablet/bin/debian-chroot.sh sudo apt-get install btop \
	locales \
	openssh-client \
	chromium \
	firefox-esr firefox-esr-mobile-config \
	maliit-inputcontext-gtk2 maliit-inputcontext-gtk3 -y
