#!/bin/bash

MCHRDIR=/home/phablet/Documents/debian
COMMAND=$@
echo "Start $COMMAND"

chroot_os(){
	sudo chroot --userspec=phablet:phablet ${MCHRDIR} \
		/usr/bin/env -i \
		HOME=/home/phablet/ \
		TERM="$TERM" \
		PS1="\[\e]0;chroot debian\u@\h: \w\a\]$PS1" \
		PATH=/usr/bin:/usr/sbin \
		DISPLAY=:0 \
		XDG_RUNTIME_DIR=/run/user/32011 \
		WAYLAND_DISPLAY=wayland-0 \
		XSOCKET=/tmp/.X11-unix/X0 \
		DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/32011/bus \
		GTK_IM_MODULE=Maliit \
		QT_IM_MODULE=maliitphablet \
		GDK_GL=gles \
		GDK_DPI_SCALE=3 \
		USER=phablet \
		PWD=/home/phablet \
		MIR_SERVER_NAME=session-0 \
		MIR_SERVER_HOST_SOCKET=/run/mir_socket \
		MIR_SERVER_FILE=/run/user/32011/mir_socket \
		XDG_SESSION_PATH=/org/freedesktop/DisplayManager/Session0 \
		XDG_SESSION_TYPE=mir \
		APP_XMIR_ENABLE=0 \
		MIR_SERVER_ENABLE_MIRCLIENT=1 \
		MIR_SERVER_ENABLE_X11=1 \
		MIR_SOCKET=/run/user/32011/mir_socket \
		MOZ_USE_XINPUT2=1 \
		LANG="$LANG" \
		$@
}

chroot_os $COMMAND