#!/bin/bash

MCHRDIR=/home/phablet/Documents/debian

mount_chroot(){
	sudo cp /etc/hosts ${MCHRDIR}/etc/hosts
	sudo cp /etc/resolv.conf ${MCHRDIR}/etc/resolv.conf

	sudo mount -o bind /dev ${MCHRDIR}/dev
	sudo mount --bind /dev/pts ${MCHRDIR}/dev/pts
	sudo mount --bind /proc  ${MCHRDIR}/proc
	sudo mount --bind /tmp  ${MCHRDIR}/tmp

	# vous pourriez en avoir besoin (par exemple dans le mode Rescue de l'installateur Debian)
	sudo mount --bind /sys  ${MCHRDIR}/sys
	sudo mount --bind /run  ${MCHRDIR}/run

	#dbus
	sudo sudo mount -o bind /var/run/dbus/ ${MCHRDIR}/var/run/dbus

	#shm (chromium)
	sudo mount --bind /dev/shm/ ${MCHRDIR}/dev/shm

	#mtab
	sudo touch ${MCHRDIR}/etc/mtab
    	sudo mount --bind /etc/mtab ${MCHRDIR}/etc/mtab

	#wayland
	mkdir -p ${MCHRDIR}/run/user/32011
	sudo mount --bind /run/user/32011 ${MCHRDIR}/run/user/32011

        #share with host
        mkdir -p /home/phablet/DebianShare
        sudo mkdir -p ${MCHRDIR}/home/phablet/DebianShare
        sudo mount --bind /home/phablet/DebianShare ${MCHRDIR}/home/phablet/DebianShare

}

mount_chroot
