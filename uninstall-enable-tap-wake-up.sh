#!/bin/bash

systemctl disable enable-tap-wake-up --user
systemctl stop enable-tap-wake-up --user

rm /home/phablet/.config/systemd/user/enable-tap-wake-up.service
rm /home/phablet/bin/enable-tap-wake-up.sh
sudo systemctl daemon-reload
