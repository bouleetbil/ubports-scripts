#!/bin/bash

mkdir -p /home/phablet/bin
mkdir -p /home/phablet/.config/systemd
mkdir -p /home/phablet/.config/systemd/user/
cp .config/systemd/user/debian-mount.service /home/phablet/.config/systemd/user/debian-mount.service
cp bin/debian-create-chroot.sh /home/phablet/bin/debian-create-chroot.sh
cp bin/debian-chroot.sh /home/phablet/bin/debian-chroot.sh
cp bin/debian-mount-chroot.sh /home/phablet/bin/debian-mount-chroot.sh

#enable and start systemctl mount chroot system
sudo systemctl daemon-reload
systemctl enable debian-mount --user
#systemctl start debian-mount --user
